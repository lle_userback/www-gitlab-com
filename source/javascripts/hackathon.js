/* global setupCountdown */

function setupHackathonCountdown() {
  var nextHackathonDate = new Date('May 29, 2019 00:00:00').getTime();

  setupCountdown(nextHackathonDate, 'nextHackathonCountdown');
}

(function() {
  setupHackathonCountdown();
})();

// Use the GitLab API to fetch the number of MRs submitted during the Hackathon
// It assumes there is a tracking issue that contains the list of MRs as
// related merge requests

// Hackathon tracker issue. Update the issue ID for each Hackathon
// E.g. on https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/18
// the issue ID is '18'
var hackathonIssueID = '14';
var apiHost = 'https://gitlab.com/api/v4';
var gitlabProjectID = '9821951';
var apiEndpoint = '/projects/' + gitlabProjectID
  + '/issues/' + hackathonIssueID + '/related_merge_requests';
var apiURL = apiHost + apiEndpoint;

fetch(apiURL)
  .then(function(response) {
    var totalRelatedMRs = response.headers.get('X-Total');
    var boxMessage = totalRelatedMRs + ' MRs submitted '
      + ' at the last Hackathon!';
    document.getElementById('hackathonMRCount').innerHTML = boxMessage;
  })
  .catch(error => console.error('Error:', error));

---
layout: markdown_page
title: "Content Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Content Marketing

The Content Marketing team includes audience development, editorial, social marketing, video production, and content strategy, development, and operations. The Content Marketing team is responsible for the stewardship of GitLab's audiences, users, customers, and partners' content needs, preferences and perceptions of GitLab. Content marketing creates engaging, inspiring, and relevant content, executing integrated content programs to deliver useful and cohesive content experiences that build trust and preference for GitLab.

Roles on the Content Marketing team include:

- [Content Editor and Managing Editor](/job-families/marketing/editor/)
- [Content Marketer](/job-families/marketing/content-marketing/)
- [Digital Production Manager](/job-families/marketing/digital-production-manager/)
- [Social marketer](/job-families/marketing/social-marketing-manager/)

## Quick links

- [Social marketing handbook](/handbook/corporate-marketing/social-marketing/)
- [GitLab blog](/handbook/marketing/blog)
- [Blog calendar](/handbook/marketing/blog/#blog-calendar)
- [Editorial review checklist](/handbook/marketing/corporate-marketing/content/editorial-review-checklist/)
- [Social media guidelines](/handbook/marketing/social-media-guidelines/)
- [Content Hack Day](/handbook/marketing/corporate-marketing/content/content-hack-day)
- [Newsletters ](/handbook/marketing/marketing-sales-development/marketing-programs/#newsletter) 
- [User spotlights](/handbook/marketing/corporate-marketing/content/user-spotlights)

## Content team responsibilities:

- Content pillar production
- Video production
- Blog mangement including writing, editing, and scheduling 
- Branded YouTube management 
- Organic branded social media mangement (Twitter, Facebook, and LinkedIn)
- Social media campaigns 

## Communication

**Chat**

Please use the following Slack channels:

- `#content` for general inquiries
- `#content-updates` for updates and log of all new, published content
- `#blog` RSS feed 
- `#twitter` for questions about social
- `#content-hack-day` for updates and information on Content Hack Day

**Issue trackers**
  - [Content Marketing](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/911769?&label_name[]=Content%20Marketing)
  - [Blog posts](https://gitlab.com/gitlab-com/www-gitlab-com/boards/804552?&label_name[]=blog%20post)

### Who to contact 

Contact [Erica](/company/team/#EricaLindberg_), Manager, Content Marketing, for questions and information regarding the content team, strategy, processes, and schedule.

**Blog & editorial**
- [Rebecca](/company/team/#rebecca), Managing Editor
- Valerie, Senior Content Editor
- [Sara](/company/team/#sarakassabian), Content Editor

**Social media & video production** 
- [Emily](/company/team/#emvonhoffmann), Social Marketing Manager
- [Aricka](/company/team/#arickaflowers), Digital Production Manager

**Content marketing** 
- [Suri](/company/team/#suripatel), Content Marketing Assoicate, Dev
- [Chrissie](/company/team/#the_chrissie), Content Marketing Manager, Ops
- Vanessa, Content Marketing Manager, Security 

## Mission & vision 

Our **content marketing mission statement** mirrors our [company mission](/company/strategy/#mission). We strive to help foster an open, transparent, and collaborative world where all digital creators can commit to becoming active participants over passive observers regardless of location, skillset, or status by sharing our community's success and learnings, helpful information and advice, and inspirational insights. 

Our **vision** is to build the largest and most diverse community of cutting edge co-conspirators who are leading the way to define and create the next generation of software development practices. We will do this by turning our corporate blog into a digital magazine and by creating helpful digital resources that people love.

## Content production process 

The content team supports many cross-functional marketing initiatives. We aim for content production to be a month ahead of planned events and campaigns so that content is readily available for use. In accordance with our values of [iteration](/handbook/values/#iteration) and [transparency](/handbook/values/#transparency), we publish our proposed content plans at the beginning of each quarter. We don't hoard content for one big launch and instead plan sets of content to execute on each quarter. Once a content set is completed, it can be compiled into a resource.

Content production is determined and prioritized based on the following:

1. Corporate GTM themes
1. Integrated campaigns
1. Corporate marketing events 
1. Newsworthiness 
1. Brand awareness opportunity 

### Content pillars

We use content pillars to plan our work so we can provide great digital experiences to our audiences. A content pillar is aligned to a high-level theme (for example, Just Commit) and executed via sets. The content team aligns to themes to ensure we are executing strategically and meeting business goals. Pillars allow us to narrow in on a specific topic and audience, and sets help us break our work into more manageable compontents. Each set created should produce an end-to-end content experience (awareness to decision) for our audience. 

**Defintions**

- Theme: A high-level GTM message that doesn't change often. Themes are tracked as `Parent Epics`.  
- Pillar: A topic within a theme. Pillars are tracked as `Child Epics`. 
- Set: A grouping of content to be executed within a specific timeframe. Sets are tracked as `Milestones`. 
- Resource: An informative asset, such as an eBook or report, that is often gated.

![Content pillar diagram](/images/handbook/marketing/corporate-marketing/content-pillar.png)

**Sources:**

- [What Is a Content Pillar? The Foundation for Efficient Content Marketing](https://kapost.com/b/content-pillar/)
- [How to Create Pillar Content Google Will Love](https://contentmarketinginstitute.com/2018/04/pillar-content-google/)
- [What Is a Pillar Page? (And Why It Matters For Your SEO Strategy)](https://blog.hubspot.com/marketing/what-is-a-pillar-page)

### Planning timeline 

When planning, we follow the 80/20 rule: 80% of capacity is planned leaving 20% unplanned to react to last minute opportunities. 

- **2 weeks prior to start of the quarter:** Proposed content plans are published to the content schedule. Product marketing, digital marketing, and corporate events marketing give feedback on the plan. 
- **1 week prior to the start of the quarter:** Kickoff calls are held. 
- **1st day of the quarter:** Content plans are finalized. 
- **1st of each month:** Progress reviews are held. Plans are adjusted if needed.
- **Last day of the quarter:** Cross-functional retrospective is held. 

## Requesting content and copy reviews

1. If you are looking for content to include in an email, send to a customer, or share on social, check the [GitLab blog](/blog/) first.
1. If you need help finding relevant content to use, ask for help in the #content channel.
1. If the content you're looking for doesn't exist and it's a:
   - Blog post: open an issue in [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com) and label it `blog post`. 
   - Digital asset (eBook, infographic, report, etc.): open an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/) and label it `content marketing`
   - Video: open an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/) and label it `video production`
1. If you need a copyedit, ping @erica. Please give at least 3 days notice.

## Messaging for Verticals
When to develop vertical messaging: The key is to determine if an industry has a certain pain point that another industry does not share.  You need to describe the problem (using industry specific terminology, if necessary) and also how your product solves these problems for them. Additionally, you can create high-level messaging and then branch off; for example if multiple industries are very security conscious, create security focused marketing, and adapt to select high value verticals.



---
layout: markdown_page
title: "The GitLab Tax Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}
 
# Keep calm and love taxes!

## Contacting the Tax Team

The tax department is responsible for GitLab’s overall tax strategy including all components of tax compliance, tax planning and accounting for income taxes. Tax regulations taxation differ between countries, which can make this area complex. The tax team is here to support you, make it simple for you and guide you through the landscape of taxes. In case you have any ad-hoc questions please feel free reach out on the #tax channel on Slack. For the sake of clarity please do not use it to seek tax advice for personal matters. We will try to to our best to answer your questions on taxation of your #stock options though. For any in-depth discussions please reach out to the team.

## Corporate Structure

Find below our corporate structure per May 2019

```
GitLab Inc
  ├── GitLab Federal LLC
  └── GitLab BV
      ├── GitLab UK
      ├── GitLab GmbH
      ├── GitLab Pty Ltd
      ├── GitLab Canada (in formation)
      ├── GitLab South Korea (in formation)
      └── GitLab Japan (in formation)
  ```
## Tax Procedures for Reporting Taxable Gains on Option Exercises

1. Options are exercised by this [procedure](https://about.gitlab.com/handbook/stock-options/#how-to-exercise-your-stock-options)
2. CFO provides options exercise file on monthly basis to Payroll & Payments Lead
3. Payroll & Payments Lead ensures wage tax compliance for employees per country as defined in the table below
4. Employee receives gain from exercise according to the common monthly salary payment procedure

 | Country                    | Description    |
|----------------------------|-----------|
| United States              | If ISO reporting is handled through filing of Form 3921 as part of year end filing |
| United Kingdom         | Difference between fair market value and exercise price is taxed at date of exercise if there is a liquid market for the stock at time of exercise. |
| Netherlands      | Difference between fair market value and exercise price is taxed at date of exercise. On monthly basis payroll to send exercise details including grant price, FMV and number of shares exercised to Dutch payroll provider. |
| Australia        | Difference between fair market value and exercise price is taxed at date of exercise. On monthly basis payroll to send exercise details including grant price, FMV and number of shares exercised to Australian payroll provider. |
| Germany          | Difference between fair market value and exercise price is taxed at date of exercise. On monthly basis payroll to send exercise details including grant price, FMV and number of shares exercised to German payroll provider. |

For additional information relating to taxation in other countries [stock option](https://drive.google.com/a/gitlab.com/file/d/1zIAUDfY-WrlPruNJgpiXl6QaOWi2p0TU/view?usp=sharing)

## Tax Procedure for the R&D Wage Tax Credit in the Netherlands ("WBSO")

In the Netherlands a tax credit applies to qualifying research and development activities ("WBSO"). In order to qualify for the WBSO companies need to file an application with the [Netherlands Enterprise Agency](https://english.rvo.nl/subsidies-programmes/wbso). In order to qualify, it needs to be proved that the R&D activities meet the following conditions:

* the proposed R&D activities take place in your own company;
* the technological development is new to your organisation;
* the development is accompanied by technical problems;
* the R&D work has yet to take place (in other words, you must always submit a WBSO application in advance).

When the application is approved, the R&D tax credit can be deducted from the wage tax due through the monthly basis payroll. The process for the WBSO from filing the application to applying the deduction in the payroll are described below.

1. GitLab appoints a dedicated engineering manager to oversee all R&D activities in the Netherlands
1. The dedicated engineering manager drafts the project description for eligible activities and estimates the R&D hours
1. The project description is reviewed by an external [consultant](https://about.gitlab.com/handbook/people-operations/#organizing-wbso), Finance and People Ops
1. Upon approval of the project description the external consultant files the application with the Netherlands Enterprise Agency
1. When the Netherlands Enterprise Agency approves the application for the WBSO, a grant letter is sent to GitLab BV
1. The grant letter has to be shared with the Dutch payroll agent
1. The Dutch payroll agent applies the WBSO in the monthly basis payroll
1. GitLab BV's employees working on the eligible projects have to keep track of their hours spent on the project in the [hour tracker](https://about.gitlab.com/handbook/people-operations/#organizing-wbso)
1. The hour tracker needs to be monitored on monthly basis by Finance in order to prevent an overclaim or underclaim of the WBSO

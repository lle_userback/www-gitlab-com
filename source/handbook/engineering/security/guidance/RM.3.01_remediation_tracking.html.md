---
layout: markdown_page
title: "RM.3.01 - Remediation Tracking Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# RM.3.01 - Remediation Tracking

## Control Statement

Management prepares a remediation plan to formally manage the resolution of findings identified in risk assessment activities.

## Context

Risk assessments find and prioritize risks, but that information and insight is only useful if it's acted on. This control aims to ensure the risks we find in risk assessments are appropriately acted on and remediation efforts are seen to their full completion.

## Scope

This control applies to all risk assessments and their respective risk findings.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.3.01_remediation_tracking.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.3.01_remediation_tracking.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.3.01_remediation_tracking.md).

## Framework Mapping

* SOC2 CC
  * CC4.2
  * CC5.1
  * CC5.2
